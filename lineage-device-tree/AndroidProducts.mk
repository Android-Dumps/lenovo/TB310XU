#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_TB310XU.mk

COMMON_LUNCH_CHOICES := \
    lineage_TB310XU-user \
    lineage_TB310XU-userdebug \
    lineage_TB310XU-eng
