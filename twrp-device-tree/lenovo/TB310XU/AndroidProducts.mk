#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_TB310XU.mk

COMMON_LUNCH_CHOICES := \
    omni_TB310XU-user \
    omni_TB310XU-userdebug \
    omni_TB310XU-eng
