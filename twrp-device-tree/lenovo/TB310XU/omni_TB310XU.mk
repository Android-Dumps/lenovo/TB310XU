#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from TB310XU device
$(call inherit-product, device/lenovo/TB310XU/device.mk)

PRODUCT_DEVICE := TB310XU
PRODUCT_NAME := omni_TB310XU
PRODUCT_BRAND := Lenovo
PRODUCT_MODEL := TB310XU
PRODUCT_MANUFACTURER := lenovo

PRODUCT_GMS_CLIENTID_BASE := android-lenovo

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="TB310XU-user 12 SP1A.210812.016 TB310XU_USR_S000525_2303101228_mp1V1457_ROW release-keys"

BUILD_FINGERPRINT := Lenovo/TB310XU/TB310XU:12/SP1A.210812.016/S000525_230310_ROW:user/release-keys
